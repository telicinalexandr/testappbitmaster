package mylayouts;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import ru.taximaster.testapp.MainActivity;
import ru.taximaster.testapp.R;

public class GridFragment extends Fragment {
    private int bkgColor;
    public int numberPage;
    private int numberCountItems;

    final static String NUMBER_PAGE = "NUMBER_PAGE";
    final static String NUMBER_COUNT_ITEMS = "NUMBER_COUNT_ITEMS";
    final static String URLS_LIST = "URLS_LIST";

    public GridAdapter gridAdapter = new GridAdapter();
    private GridView gridView;

    private ArrayList<String> listUrls;

    public static GridFragment getNewInstance(int numberCountItems, int numberPage, ArrayList<String> urls) {
        GridFragment gf = new GridFragment();
        Bundle args = new Bundle();
        args.putInt(NUMBER_PAGE, numberPage);
        args.putInt(NUMBER_COUNT_ITEMS, numberCountItems);
        args.putStringArrayList(URLS_LIST, urls);
        gf.setArguments(args);
        return gf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Random rnd = new Random();
        bkgColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        numberPage = getArguments().getInt(NUMBER_PAGE);
        numberCountItems = getArguments().getInt(NUMBER_COUNT_ITEMS);
        listUrls = getArguments().getStringArrayList(URLS_LIST);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_fragment, null);
        view.setBackgroundColor(bkgColor);
        gridView = (GridView) view.findViewById(R.id.grid);
        gridView.setAdapter(gridAdapter);
        return view;
    }

    public void openShowImageActivity(String url) {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).openShowImageActivity(url);
        }
    }

    public void updateData(ArrayList<String> list) {
        if (list != null) {
            listUrls = list;
            gridAdapter.notifyDataSetChanged();
        }
    }

    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return numberCountItems;
        }

        @Override
        public Object getItem(int i) {
            return listUrls.get(i + numberCountItems * numberPage);
        }

        @Override
        public long getItemId(int i) {
            return i + numberCountItems * numberPage;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            int index = i + numberCountItems * numberPage;
            ImageView imageView;
            if (view != null) {
                imageView = (ImageView) view;
            } else {
                imageView = new ImageView(viewGroup.getContext());
            }

            if (listUrls.size() > 0 && index < listUrls.size()) {
                try {
                    String url = listUrls.get(index);
                    if (!TextUtils.isEmpty(url)) {
                        imageView.setTag(url);
                        Picasso.get().load(url).into(imageView);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                openShowImageActivity(v.getTag().toString());
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return imageView;
        }
    }
}
