package ru.taximaster.testapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.ArrayList;

import mylayouts.GridFragment;

public class MainActivity extends AppCompatActivity implements IAsyncResponse{

    public static final int PAGE_COUNT = 4;
    public static final int PAGE_COUNT_ITEMS = 10;

    private EditText editTextSearch;
    private ArrayList<String> listUrls = new ArrayList<String>();

    private ProgressBar progressBar;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextSearch = (EditText) findViewById(R.id.editText);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        btnSearch = (Button) findViewById(R.id.btn_search);

        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_search);
    }

    public void onClickSearch(View view) {
        btnSearch.setEnabled(false);
        progressBar.setAlpha(1);
        runDownloadImages(viewPager.getCurrentItem() + 1);
    }

    private void runDownloadImages(int numberPage) {
        String text = "";
        if (editTextSearch != null) {
            text = editTextSearch.getText().toString();
        }

        UrlsImagesLoader imageLoader = new UrlsImagesLoader(PAGE_COUNT_ITEMS, PAGE_COUNT);
        imageLoader.delegate = this;
        imageLoader.execute(text);
    }

    public void openShowImageActivity(String url) {
        Intent intent = new Intent(this, ShowImageActivity.class);
        intent.putExtra(ShowImageActivity.EXTRA_URL_IMAGE, url);
        startActivity(intent);
    }


    public void onClickOpenMaps(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
//        intent.putExtra(ShowImageActivity.EXTRA_URL_IMAGE, url);
        startActivity(intent);
    }

    @Override
    public void Update(ArrayList<String> listUrls) {
        this.listUrls = listUrls;
        progressBar.setAlpha(0);
        btnSearch.setEnabled(true);
        pagerAdapter.notifyDataSetChanged();
    }


    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return GridFragment.getNewInstance(PAGE_COUNT_ITEMS, position, listUrls);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            ((GridFragment) object).updateData(listUrls);
            return super.getItemPosition(object);
        }
    }
}