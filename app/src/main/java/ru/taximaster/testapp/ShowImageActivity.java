package ru.taximaster.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ShowImageActivity extends AppCompatActivity {
    public static final String EXTRA_URL_IMAGE = "EXTRA_URL_IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        ImageView img = (ImageView) findViewById(R.id.img);
        String url = getIntent().getStringExtra(EXTRA_URL_IMAGE);
        Picasso.get().load(url).into(img);
    }
}
