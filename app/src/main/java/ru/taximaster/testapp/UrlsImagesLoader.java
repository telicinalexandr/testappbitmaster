package ru.taximaster.testapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlsImagesLoader extends AsyncTask<String, Integer, ArrayList<String>> {

    private int countItemsOnePage;
    private int countPages;

    public IAsyncResponse delegate = null;

    public UrlsImagesLoader(int countItemsOnePage, int countPages) {
        this.countItemsOnePage = countItemsOnePage;
        this.countPages = countPages;
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        String[] questRaw = params[0].split(" ");
        String quest = questRaw[0];
        for (int i = 1; i < questRaw.length; i++) {
            quest = quest + "+" + questRaw[i];
        }

        StringBuilder builderURL = new StringBuilder();
        builderURL.append("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=")
                .append(BuildConfig.KEY_FICKER)
               // .append("&lat=55.75&lon=37.61&") // Москва
                .append("&sort=relevance&content_type=1&per_page=")
                .append((countItemsOnePage * countPages))
                .append("&media=photos&format=json&text='")
                .append(quest)
                .append("'");

        Log.v("TAG","URL - "+builderURL);

        String requestResult = requestToServer(builderURL.toString());
        return parseResultToListUrls(requestResult);
    }

    @Override
    protected void onPostExecute(ArrayList<String> urlLists) {
        super.onPostExecute(urlLists);
        if(delegate != null){
            delegate.Update(urlLists);
        }
    }

    // Запрос к серверу на получене картинок
    public String requestToServer(String reqURL) {
        String line = "";
        try {
            URL url = new URL(reqURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            line = reader.readLine();
            con.disconnect();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return line;
    }

    // Преобразовыывает полученный результат с сервера в список с URLS картинок
    public ArrayList<String> parseResultToListUrls(String requestResult) {
        Log.v("TAG","[parseResultToListUrls] result - "+requestResult);
        String jsonStr = null;
        JSONObject json = null;
        JSONArray photo = null;
        ArrayList<String> list = new ArrayList<String>();

        // line обернуто в jsonFlickrApi(json)
        Pattern p = Pattern.compile(".*?\\((.*)\\)$");
        Matcher m = p.matcher(requestResult);

        if (m.matches()) {
            jsonStr = m.group(1);
        }

        if (jsonStr != null) {
            try {
                json = new JSONObject(jsonStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            photo = json.getJSONObject("photos").getJSONArray("photo");
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < photo.length(); i++) {
            try {
                JSONObject photos = photo.getJSONObject(i);

                String farm = photos.getString("farm");
                String server = photos.getString("server");
                String id = photos.getString("id");
                String secret = photos.getString("secret");

                StringBuilder url = new StringBuilder();
                url.append("http://farm")
                        .append(farm)
                        .append(".static.flickr.com/")
                        .append(server)
                        .append("/")
                        .append(id)
                        .append("_")
                        .append(secret)
                        .append(".jpg");

                list.add(url.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}